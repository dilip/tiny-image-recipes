#!/usr/bin/env groovy

release = "18.09"

upload_dest = "archive@images.apertis.org:/srv/images/public"
upload_credentials = '5a23cd79-e26d-41bf-9f91-d756c131b811'

def uploadDirectory(source, target, upload = true) {

  if (!upload) {
    println "Skipping upload of ${source} to ${target}"
    return
  }

  sshagent (credentials: [ upload_credentials, ] ) {
    env.NSS_WRAPPER_PASSWD = "/tmp/passwd"
    env.NSS_WRAPPER_GROUP = '/dev/null'
    sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
    sh(script: "LD_PRELOAD=libnss_wrapper.so rsync -e \"ssh -oStrictHostKeyChecking=no\" -aP ${source} ${upload_dest}/${target}/")
  }
}

def runTestsJobs(version, release, type, arch, board, submit = true) {

  if (!submit) {
    println "Skipping submitting tests jobs"
    return
  }

  // TODO: Remove "uefi" once images run in the actual boards
  def boardp = (board in ["uefi", "sdk"]) ? "qemu" : board

  withCredentials([ file(credentialsId: 'apertis-lava-user', variable: 'lqaconfig'),
                    usernamePassword(credentialsId: 'apertis-lava',
                                     usernameVariable: 'username',
                                     passwordVariable: 'password') ]) {
    // Need to pass credentials only for `mx6qsabrelite` board
    // Use empty credentials for public access
    def credentials = ""
    if( arch == "armhf" ) {
      credentials="${username}:${password}@"
    }
    sh(script: """\
    /usr/bin/lqa -c ${lqaconfig} submit \
    --profile lxc-${release}-${type}-${arch}-${boardp} \
    -g lavatests/profiles.yaml \
    -t release:${release} \
    -t credentials:'${credentials}' \
    -t image_date:${version}""")
  }

}

def buildImage(architecture, type, board, debosarguments = "", ipk, production = false) {
  return {
    node("docker-slave") {
      checkout scm
      docker.withRegistry('https://docker-registry.apertis.org') {
        buildenv = docker.image("docker-registry.apertis.org/apertis/apertis-${release}-image-builder")
        /* Pull explicitly to ensure we have the latest */
        buildenv.pull()

        buildenv.inside("--device=/dev/kvm") {
          stage("setup ${architecture} ${type}") {
            env.PIPELINE_VERSION = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')
            sh ("env ; mkdir -p ${PIPELINE_VERSION}/${architecture}/${type}")
          }

          try {
            stage("${architecture} ${type} tiny container") {
              sh(script: """\
                  cd ${PIPELINE_VERSION}/${architecture}/${type}; \
                  debos ${debosarguments} \
                  -t type:${type} \
                  -t architecture:${architecture} \
                  -t suite:${release} \
                  -t timestamp:${PIPELINE_VERSION} \
                  -t ipk:${ipk} \
                  -t rfs:isolation_${type}_${PIPELINE_VERSION}_${architecture}.tar.gz \
                  ${WORKSPACE}/tiny-connectivity-container.yaml""")
            }

            stage("${architecture} ${type} tiny container upload") {
                uploadDirectory (env.PIPELINE_VERSION, "daily/experimental/${release}", production)
            }

            stage("Submitting tests jobs (lava)") {
                runTestsJobs (env.PIPELINE_VERSION, release, type, architecture, board, production)
            }
          } finally {
              stage("Cleanup ${architecture} ${type}") {
                  deleteDir()
              }
          }
        }
    }
  }
}


/* Determine whether to run uploads based on the prefix of the job name; in
 * case of apertis we expect the official jobs under apertis-<release>/ while
 * non-official onces can be in e.g. playground/ */
def production = env.JOB_NAME.startsWith("apertis-")

def images = [:]

// Types for all boards, common debos arguments
def  types = [ [ "rfs", "", true ],
               [ "sysroot", "", false ]
]

images += types.collectEntries { [ "Amd64 ${it[0]}": buildImage("amd64",
                                   it[0],
                                   "uefi",
                                   it[1],
                                   it[2],
                                   production ) ] }

images += types.collectEntries { [ "Arm64 ${it[0]}": buildImage("arm64",
                                   it[0],
                                   "uboot",
                                   it[1],
                                   it[2],
                                   production ) ] }

images += types.collectEntries { [ "Armhf ${it[0]}": buildImage("armhf",
                                   it[0],
                                   "uboot",
                                   it[1],
                                   it[2],
                                   production ) ] }

parallel images

#!/bin/sh
# This script generates the AppArmor profile for use in lxc-tiny-connectivity
# template.
# Usage: ./generate_profile.sh, git commit, git push

set -eu

apparmor_profile="lxc-tiny-connectivity-profile-template"
NAMESPACE="__NAMESPACE_PLACEHOLDER__"

cat > "$apparmor_profile" <<EOF
#include <tunables/global>

# empty profile wich allows to create the namespace on system-wide level
# without usage of "mkdir /sys/kernel/security/apparmor/policy/namespaces/\$NAMESPACE"
profile :$NAMESPACE:$NAMESPACE {}

profile lxc-container-apertis-$NAMESPACE flags=(attach_disconnected,mediate_deleted) {
EOF
cat >> "$apparmor_profile" tiny-container-base.in

cat >> "$apparmor_profile" <<EOF
  deny @{PROC}/kmem rwklx,
  deny @{PROC}/mem rwklx,
  audit /sys/kernel/security/apparmor/** rwklix,
  audit /sys/kernel/debug/{,**} rwklix,
EOF

#
# deny the container writes on its rootfs
#
append_to_profile() {
	echo "  $*" >> "$apparmor_profile"
}

# Prevent any form of writing in /
append_to_profile "deny / wkl,"
append_to_profile "deny /* wkl,"
append_to_profile "deny /*/ wkl,"

# Be more agressive in the following folders
for path in usr etc bin sbin boot; do
  append_to_profile "deny /$path/. wkl,"
  append_to_profile "deny /$path/** wkl,"
done

# Still allow writing in /var and /run
for path in var run; do
  append_to_profile "/$path/. rwkl,"
done


# Not include here because it is GPL licensed
# git clone https://github.com/lxc/lxc.git lxc-git
# ~/lxc/config/apparmor/lxc-generate-aa-rules.py tiny-container-rules.base >> tiny-container-rules
cat >> "$apparmor_profile" tiny-container-rules
cat >> "$apparmor_profile" <<EOF
  # Allow subprocesses to change to any namespaced profile
  change_profile -> :$NAMESPACE://*,

  # AA_PROFILE_NESTING (similar to lxc-default-with-nesting)
  deny /dev/.lxc/proc/** rw,
  deny /dev/.lxc/sys/** rw,
  mount fstype=proc -> /var/cache/lxc/**,
  mount fstype=sysfs -> /var/cache/lxc/**,
  mount options=(rw,bind),
  mount fstype=cgroup -> /sys/fs/cgroup/**,

  # AA_PROFILE_UNPRIVILEGED
  mount options=(rw,make-slave) -> **,
  mount options=(rw,make-rslave) -> **,
  mount options=(rw,make-shared) -> **,
  mount options=(rw,make-rshared) -> **,
  mount options=(rw,make-private) -> **,
  mount options=(rw,make-rprivate) -> **,
  mount options=(rw,make-unbindable) -> **,
  mount options=(rw,make-runbindable) -> **,

  mount options=(rw,bind),
  mount options=(rw,rbind),
}
EOF

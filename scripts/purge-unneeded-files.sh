#!/bin/sh
set -ve

# Crush into a minimal production image to be deployed via some type of image
# updating system. The Debian system is not longer functional at this point.

UNNEEDED_DIRS="
var/*
opt
srv
usr/share/locale
usr/share/man
usr/share/doc
usr/share/ca-certificates
usr/share/bash-completion
usr/share/zsh/vendor-completions
usr/share/gcc-5
etc/apparmor.d/abstractions/ubuntu-browsers.d
etc/apt
etc/dpkg
etc/init.d
etc/rc[0-6S].d
etc/init
lib/systemd/system-sleep
lib/systemd/system-shutdown
usr/games
usr/lib/kernel
usr/lib/gcc/arm-linux-gnueabihf
usr/share/common-licenses
usr/share/lintian
usr/lib/xtables
usr/lib/locale/*
usr/lib/systemd/boot
usr/lib/systemd/catalog
usr/lib/lsb
usr/local/man
usr/local/games
usr/local/share/man
"

UNNEEDED_FILES="
bin/chaiwala-tar
bin/networkctl
bin/pidof
bin/tar
etc/apparmor.d/abstractions/apache2-common
etc/apparmor.d/abstractions/cups-client
etc/apparmor.d/abstractions/fonts
etc/apparmor.d/abstractions/launchpad-integration
etc/apparmor.d/abstractions/mysql
etc/apparmor.d/abstractions/nvidia
etc/apparmor.d/abstractions/samba
etc/apparmor.d/abstractions/smbpass
etc/apparmor.d/abstractions/ssl_keys
etc/apparmor.d/abstractions/svn-repositories
etc/apparmor.d/abstractions/ubuntu-bittorrent-clients
etc/apparmor.d/abstractions/ubuntu-browsers
etc/apparmor.d/abstractions/ubuntu-console-browsers
etc/apparmor.d/abstractions/ubuntu-console-email
etc/apparmor.d/abstractions/ubuntu-email
etc/apparmor.d/abstractions/ubuntu-feed-readers
etc/apparmor.d/abstractions/ubuntu-gnome-terminal
etc/apparmor.d/abstractions/ubuntu-helpers
etc/apparmor.d/abstractions/ubuntu-konsole
etc/apparmor.d/abstractions/ubuntu-media-players
etc/apparmor.d/abstractions/ubuntu-unity7-base
etc/apparmor.d/abstractions/ubuntu-unity7-launcher
etc/apparmor.d/abstractions/ubuntu-unity7-messaging
etc/apparmor.d/abstractions/ubuntu-xterm
etc/apparmor.d/abstractions/wayland
etc/apparmor.d/abstractions/web-data
etc/apparmor.d/abstractions/X
lib/systemd/system/bluetooth.target
lib/systemd/system/bootlogd.service
lib/systemd/systemd-backlight
lib/systemd/systemd-modules-load
lib/systemd/system-generators/systemd-cryptsetup-generator
lib/systemd/system-generators/systemd-debug-generator
lib/systemd/system-generators/systemd-gpt-auto-generator
lib/systemd/system-generators/systemd-hibernate-resume-generator
lib/systemd/system-generators/systemd-rc-local-generator
lib/systemd/system-generators/systemd-system-update-generator
lib/systemd/system-generators/systemd-sysv-generator
lib/systemd/system/dev-hugepages.mount
lib/systemd/system/graphical.target
lib/systemd/system/initrd-cleanup.service
lib/systemd/system/initrd-fs.target
lib/systemd/system/initrd-parse-etc.service
lib/systemd/system/initrd-root-device.target
lib/systemd/system/initrd-root-fs.target
lib/systemd/system/initrd-switch-root.service
lib/systemd/system/initrd-switch-root.target
lib/systemd/system/initrd.target
lib/systemd/system/initrd-udevadm-cleanup-db.service
lib/systemd/system/module-init-tools.service
lib/systemd/system/mountnfs.service
lib/systemd/system/reboot.service
lib/systemd/system/rpcbind.target
lib/systemd/system/sys-kernel-config.mount
lib/systemd/system/sys-kernel-debug.mount
lib/systemd/system/systemd-hibernate-resume@.service
lib/systemd/system/systemd-hybrid-sleep.service
lib/systemd/system/systemd-modules-load.service
lib/systemd/system/systemd-poweroff.service
lib/systemd/system/umountnfs.service
sbin/fsck.nfs
sbin/installkernel
sbin/poweroff
sbin/shutdown
usr/bin/bootctl
usr/bin/busctl
usr/bin/hostnamectl
usr/bin/localectl
usr/bin/localedef
usr/bin/pg
usr/bin/slabtop
usr/bin/systemd-cat
usr/bin/systemd-cgls
usr/bin/systemd-cgtop
usr/bin/systemd-delta
usr/bin/systemd-detect-virt
usr/bin/systemd-path
usr/bin/systemd-run
usr/bin/systemd-socket-activate
usr/bin/watch
usr/lib/arm-linux-gnueabihf/glib-2.0/glib-compile-resources
usr/lib/arm-linux-gnueabihf/glib-2.0/glib-compile-schemas
usr/lib/iptables/iptables.init
usr/lib/systemd/user/graphical-session.target
usr/lib/systemd/user/printer.target
usr/lib/tmpfiles.d/dbus.conf
usr/lib/tmpfiles.d/debian.conf
usr/lib/tmpfiles.d/home.conf
usr/lib/tmpfiles.d/tmp.conf
usr/sbin/chpasswd
usr/sbin/*fdisk
usr/sbin/pam-auth-update
usr/sbin/useradd
usr/sbin/usermod
usr/sbin/visudo
"

# Removing unused directories
for DIR in ${UNNEEDED_DIRS}
do
	echo "Forcing removal of the directory '${DIR}'"
	if ! rm -rf "${DIR}"
	then
		echo "WARNING: Failed to remove '${DIR}'"
	fi
done

# Removing unused files
for FILE in ${UNNEEDED_FILES}
do
	echo "Forcing removal of the file '${FILE}'"
	if ! rm -f "${FILE}"
	then
		echo "WARNING: Failed to remove '${FILE}'"
	fi
done

echo "Find and remove systemd hw database manager and fuse files"
find usr etc -name '*systemd-hwdb*' -prune -delete;
find usr etc -name '*fuse*' -prune -delete;

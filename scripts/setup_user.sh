#!/bin/sh

set -e

echo "I: create user"
adduser --gecos User --disabled-login user

echo "I: set user password"
echo "user:user" | chpasswd

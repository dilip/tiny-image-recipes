#!/bin/sh

set -eu

prefix="/lib/systemd/system"

services='
dev-mqueue.mount
console-getty.service
container-getty@.service
systemd-journald-audit.socket
'
for service in $services; do
    test -f $prefix/$service && rm -f $prefix/$service
done
